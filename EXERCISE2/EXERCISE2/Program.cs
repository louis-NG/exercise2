﻿using System;

namespace EXERCISE2
{
    class Program
    {
        static int checkSNT(int x)
        {
            if (x < 2)
                return 0;
            else
            {
                for (int i = 2; i <= Math.Sqrt(x); i++)
                {
                    if (x % i == 0)
                        return 0;
                }
                return 1;
            }
        }
        static void Main(string[] args)
        {
            Console.Write("Enter the number n = ");
            int n = Int32.Parse(Console.ReadLine());
            Console.Write("Array primzahlen less than " + n + ": [ ");
            for (int i = 0; i < n; i++)
            {
                if (checkSNT(i) == 1)
                {
                    Console.Write(i+",");
                }
            }
            Console.Write(" ]");

        }
    }
}
